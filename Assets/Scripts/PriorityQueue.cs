﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PriorityQueue<T>
{

    class Node
    {
        public int priority;
        public T data;
    }


    private List<Node> nodes = new List<Node>();

    public void Insert(int priority, T data)
    {
        int current = nodes.Count;
        nodes.Add(new Node { priority = priority, data = data });

        while (current > 0)
        {
            int parent = (current - 1) / 2;

            if (nodes[current].priority >= nodes[parent].priority)
            {
                return;
            }

            Node t = nodes[parent];
            nodes[parent] = nodes[current];
            nodes[current] = t;

            current = parent;
        }
    }

    public int Count
    {
        get
        {
            return nodes.Count;
        }
    }

    public T DeleteMin()
    {
        if (Count == 0)
            return default(T);

        T obj = nodes[0].data;
        
        nodes[0] = nodes[nodes.Count - 1];
        nodes.RemoveAt(nodes.Count - 1);

        int current = 0;
        int child = 1; // left child by default
        while (child < nodes.Count)
        {
            if (child < nodes.Count - 1 && nodes[child].priority > nodes[child + 1].priority)
            {
                child = child + 1; // use right child
            }

            if (nodes[current].priority > nodes[child].priority)
            {
                Node t = nodes[current];
                nodes[current] = nodes[child];
                nodes[child] = t;
                current = child;
            }
            else
            {
                break;
            }

            child = current * 2 + 1; // left child by default
        }
        
        return obj;
    }

}
