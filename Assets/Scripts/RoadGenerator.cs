﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadGenerator : MonoBehaviour
{
    protected class Segment : SAT.Segment
    {
        public float theta;
        public bool isHighway;
        public int id;

        public LineRenderer renderer;
    }

    protected class Road
    {
        public int t;
        public Vector2 startPoint;
        public Vector2 endPoint;
        public float theta;
        public float length;
        public int branchCountDown;
        public bool isHighway;
        public bool merged;

        public int id { get; private set; }
        private static int count = 0;

        public int parent;

        public Road()
        {
            t = 0;
            branchCountDown = 0;
            isHighway = false;
            merged = false;
            id = count++;
            parent = 0;
        }


        public void CalcEndPoint()
        {
            endPoint = startPoint + length * (new Vector2(Mathf.Cos(theta), Mathf.Sin(theta)));
        }

        public void CalcThetaAndLength()
        {
            Vector2 delta = endPoint - startPoint;
            length = delta.magnitude;
            delta.Normalize();
            theta = - Mathf.Deg2Rad * Quaternion.FromToRotation(Vector3.right, new Vector3(delta.x, 0.0f, delta.y)).eulerAngles.y;
            //Debug.Log("start point = " + startPoint + ", end point = " + endPoint + ", theta = " + theta);
        }
    }


    //protected List<Vector2> points = new List<Vector2>();
    protected List<Segment> segments = new List<Segment>();
    protected List<SAT.Rect> allotments = new List<SAT.Rect>();

    protected PriorityQueue<Road> queue = new PriorityQueue<Road>();

    public bool useSeed = true;
    public int seed = 0;

    public Vector3 origin;
    public float width = 1.0f;
    public float height = 1.0f;

    public float highwaySegmentLegnth = 0.05f;
    public float highwayMergeRadius = 0.03f;
    public int highwayBranchingMinInterval = 5;
    public float highwayBranchingPopulation = 0.15f;
    public float highwayBranchingProbability = 0.2f;
    public float highwayBranchingAngleRange = 15;

    public float streetSegementLength = 0.04f;
    public float streetBranchingPopulation = 0.30f;
    public float streetBranchingProbability = 0.4f;
    public float streetBranchingAngleRange = 5;

    public float baseAllotmentSize = 0.01f;
    public float bigAllotmentPopThreshold = 0.2f;
    public float bigAllotmentCoeff = 10;

    public int mapWidth = 512;
    public int mapHeight = 512;

    public Renderer populationTex;
    public LineRenderer lineTemplate;
    public GameObject allotmentTemplate;

    private FloatMap populationMap;

    private void Reset()
    {
        //points = new List<Vector2>();
        segments = new List<Segment>();
        allotments = new List<SAT.Rect>();

        queue = new PriorityQueue<Road>();
    }

    private void Init()
    {
        Vector2 startPoint = new Vector2(width * Random.value, height * Random.value);

        float theta = 0.0f;
        Vector2 endPoint = Vector2.zero;
        do
        {
            theta = Mathf.Repeat(Random.value, Mathf.PI * 2);
            endPoint = startPoint + highwaySegmentLegnth * (new Vector2(Mathf.Cos(theta), Mathf.Sin(theta)));
        }
        while (endPoint.x < 0.0f || endPoint.x > width || endPoint.y < 0.0f || endPoint.y > height);

        Road r = new Road();
        r.t = 0;
        r.startPoint = startPoint;
        r.endPoint = endPoint;
        r.theta = theta;
        r.length = highwaySegmentLegnth;
        r.isHighway = true;

        queue.Insert(r.t, r);
    }

    private bool Step()
    {
        if (queue.Count == 0)
            return false;

        Road r = queue.DeleteMin();

        if (LocalConstraints(r))
        {
            // add segment
            Segment seg = new Segment() { end0 = r.startPoint, end1 = r.endPoint, theta = r.theta, isHighway = r.isHighway, id = r.id, renderer = null };
            segments.Add(seg);

            // add line renderer in scene
            if (lineTemplate)
            {
                LineRenderer line = Instantiate(lineTemplate);
                line.SetPosition(0, origin + new Vector3(r.startPoint.x, 0.0f, r.startPoint.y));
                line.SetPosition(1, origin + new Vector3(r.endPoint.x, 0.0f, r.endPoint.y));
                if (!r.isHighway)
                {
                    line.startWidth = line.startWidth * 0.6f;
                    line.endWidth = line.endWidth * 0.6f;
                }
                line.gameObject.name = lineTemplate.gameObject.name + "_" + r.id;
                line.enabled = true;
                seg.renderer = line;

                //Debug.Log("id = " + r.id);
            }

            // add new roads
            GlobalGoal(r);
        }

        return true;
    }

    //void Start()
    IEnumerator Start()
    {
        var backupState = Random.state;

        if (useSeed)
        {
            Random.InitState(seed);
        }

        populationMap = new FloatMap(mapWidth, mapHeight);

        float perlinOriginX = 1.0f;
        float perlinOriginY = 1.0f;
        float perlinScale = 2.0f;

        populationMap.FillInPerlinNoise(perlinScale, perlinScale, perlinOriginX, perlinOriginY, (x) => Mathf.Pow(x * 0.5f + 0.5f, 4));
        //heightMap.fil

        if (populationTex)
        {
            Material mat = new Material(populationTex.sharedMaterial);
            populationTex.material = mat;

            Texture2D tex = populationMap.CreateTexture(new Color(0, 1, 0));

            tex.filterMode = FilterMode.Point;

            mat.mainTexture = tex;
        }

        Reset();
        Init();

        yield return null;

        while (Step())
        {
            yield return null;
            //yield return new WaitForSeconds(0.2f);
        }

        yield return null;

        yield return PlaceBuildings();

        Debug.Log("Done.");

        Random.state = backupState;
    }

    protected virtual bool LocalConstraints(Road r)
    {
        // environmental constraints

        // map edge
        if (!InsideMap(r.endPoint))
        {
            r.length *= 0.5f;
            r.CalcEndPoint();
            if (!InsideMap(r.endPoint))
            {
                return false;
            }
        }

        // TODO
        // water ?
        // height ?


        // try find a nearest point where r intersects with existing roads
        Segment nearestIntersected = null;
        Vector3 nearestIntersection = Vector3.zero;
        float nearestIntersectionDist = float.MaxValue;
        foreach (Segment seg in segments)
        {
            if (r.parent == seg.id)
                continue;

            Vector2 intersection = Vector2.zero;
            if (Intersect(r.startPoint, r.endPoint, seg.end0, seg.end1, out intersection))
            {
                if (Mathf.Abs(Vector2.Dot(
                    (r.endPoint - r.startPoint).normalized, 
                    (seg.end1 - seg.end0).normalized)) > 0.8f)
                {
                    return false;
                }

                float dist = (intersection - r.startPoint).magnitude;
                if (dist < nearestIntersectionDist)
                {
                    nearestIntersectionDist = dist;
                    nearestIntersection = intersection;
                    nearestIntersected = seg;
                }
            }
        }

        if (null != nearestIntersected)
        {
            // if we found a intersection
            // cut the road to the intersection point
            r.endPoint = nearestIntersection;
            r.CalcThetaAndLength();
            if (r.length < highwayMergeRadius)
            {
                return false;
            }
            r.merged = true;

            Segment seg = nearestIntersected;
            Segment seg1 = new Segment() { end0 = nearestIntersection, end1 = seg.end1, theta = seg.theta, isHighway = seg.isHighway, id = seg.id };
            nearestIntersected.end1 = nearestIntersection;
            segments.Add(seg1);
            // TODO update linerenderer

        }
        else
        {
            // try snap the road to existing roads or intersections
            foreach (Segment seg in segments)
            {
                Vector2 s = (seg.end1 - seg.end1);
                Vector2 segDir = s.normalized;
                Vector2 a = (r.endPoint - seg.end0);
                Vector2 b = (r.endPoint - seg.end1);

                float distToEnd0 = a.magnitude;
                float distToEnd1 = b.magnitude;

                if (distToEnd0 < highwayMergeRadius)
                {
                    r.endPoint = seg.end0;
                    r.CalcThetaAndLength();
                    r.merged = true;
                    break;
                }
                else if (distToEnd1 < highwayMergeRadius)
                {
                    r.endPoint = seg.end1;
                    r.CalcThetaAndLength();
                    r.merged = true;
                    break;
                }
                else
                {
                    // if the end point's projection is on the segement
                    float proj = Vector2.Dot(segDir, a);
                    if (proj > 0.0f && proj < s.magnitude)
                    {
                        float distProj = (a - proj * segDir).magnitude;
                        // if the end point is near the segment
                        if (distProj < highwayMergeRadius)
                        {
                            r.endPoint = seg.end0 + proj * segDir;
                            r.CalcThetaAndLength();
                            r.merged = true;

                            Segment seg1 = new Segment() { end0 = r.endPoint, end1 = seg.end1, theta = seg.theta, isHighway = seg.isHighway, id = seg.id };
                            seg.end1 = r.endPoint;
                            segments.Add(seg1);
                            // TODO update linerenderer

                            break;
                        }
                    }
                }
            }
        }

        return true;
    }

    protected virtual void GlobalGoal(Road r0)
    {
        // stop if this road has merged into other existing roads
        if (r0.merged)
        {
            return;
        }

        // extrude and branch
        ExtrudeRoads(r0);

        // if this is the first road, also start from another direction
        if (r0.t == 0)
        {
            ExtrudeRoads(r0, 0, true);
        }

    }

    private bool InsideMap(Vector2 pos)
    {
        return !(pos.x < 0 || pos.x > width || pos.y < 0.0f || pos.y > height);
    }

    private float SamplePopulation(Vector2 pos, float theta, float length)
    {
        Vector2 dir = new Vector2(Mathf.Cos(theta), Mathf.Sin(theta));

        float sample = 0.0f;

        for (int j = 0; j < 10; j++)
        {
            Vector2 samplePoint = pos + j / 10.0f * length * dir;

            float p = populationMap.Sample(samplePoint.x / width, samplePoint.y / height, 0.0f);

            sample += 1.0f / (10 - j) * p;
        }

        //Debug.Log(sample);

        return sample;
    }

    private float DiverseDirectionRandomly(float theta, float randomDirRange)
    {
        float randomDelta = Random.Range(-randomDirRange, +randomDirRange);
        return Mathf.Repeat(theta + randomDelta, Mathf.PI * 2);
    }

    // assuming they are in the same xz-plane
    private bool Intersect(Vector2 a0, Vector2 b0, Vector2 a1, Vector2 b1, out Vector2 intersection)
    {
        // | a  b |  *  | t0 |  =  | e |
        // | c  d |     | t1 |  =  | f |

        Vector2 v0 = (b0 - a0);
        Vector2 v1 = (b1 - a1);

        float
            a = v0.x,
            b = -v1.x,
            c = v0.y,
            d = -v1.y,
            e = a1.x - a0.x,
            f = a1.y - a0.y;

        float det = a * d - b * c;

        if (Mathf.Abs(det) < 0.0001f)
        {
            intersection = Vector2.zero;
            return false;
        }

        float det0 = e * d - b * f;
        float det1 = a * f - e * c;

        float t0 = det0 / det;
        float t1 = det1 / det;

        if (t0 <= 0.0f || t0 >= 1.0f || t1 <= 0.0f || t1 >= 1.0f)
        {
            intersection = Vector2.zero;
            return false;
        }

        intersection = a0 + t0 * v0;
        return true;
    }

    private void ExtrudeRoads(Road r0, int delay = 0, bool fromStartPoint = false)
    {
        Road r = new Road();

        // inherit parameters from r0
        r.t = r0.t + 1 + delay;
        r.startPoint = r0.endPoint;
        r.length = r0.length;
        r.branchCountDown = r0.branchCountDown > 0 ? (r0.branchCountDown - 1) : 0;
        r.isHighway = r0.isHighway;
        r.parent = r0.id;

        float initialTheta = r0.theta;

        if (fromStartPoint)
        {
            r.startPoint = r0.startPoint;
            initialTheta = Mathf.Repeat(r0.theta + Mathf.PI, Mathf.PI * 2);
        }

        // sample population on the map
        float straightSample = SamplePopulation(r.startPoint, initialTheta, r.length);

        float highwayRandomDirRange = highwayBranchingAngleRange / 180.0f * Mathf.PI;
        float streetRandomDirRange = streetBranchingAngleRange / 180.0f * Mathf.PI;
        float randomDirRange = r0.isHighway ? highwayRandomDirRange : streetRandomDirRange;

        float randomTheta = DiverseDirectionRandomly(initialTheta, randomDirRange);
        float randomDirSample = SamplePopulation(r.startPoint, randomTheta, r.length);

        float populationSample = 0.0f;

        // chose the direction with higher population
        if (randomDirSample > straightSample)
        {
            r.theta = randomTheta;
            populationSample = randomDirSample;
        }
        else
        {
            r.theta = initialTheta;
            populationSample = straightSample;
        }

        r.CalcEndPoint();

        if (r.isHighway || populationSample > streetBranchingPopulation)
        {
            // add the new road to the queue
            queue.Insert(r.t, r);
        }

        if (r0.isHighway)
        {
            // make highway branches
            if (r.branchCountDown == 0 && populationSample > highwayBranchingPopulation)
            {
                if (Random.value < highwayBranchingProbability)
                {
                    // make a left branch
                    Road leftBr = new Road();
                    leftBr.t = r.t + 1;
                    leftBr.parent = r0.id;
                    leftBr.startPoint = r.startPoint;
                    leftBr.length = highwaySegmentLegnth;
                    leftBr.isHighway = true;
                    leftBr.theta = DiverseDirectionRandomly(initialTheta + Mathf.PI * 0.5f, highwayRandomDirRange);
                    leftBr.CalcEndPoint();

                    leftBr.branchCountDown = highwayBranchingMinInterval;
                    r.branchCountDown = highwayBranchingMinInterval;

                    queue.Insert(leftBr.t, leftBr);
                }
                else if (Random.value < highwayBranchingProbability)
                {
                    // make a right branch
                    Road rightBr = new Road();
                    rightBr.t = r.t + 1;
                    rightBr.parent = r0.id;
                    rightBr.startPoint = r.startPoint;
                    rightBr.length = highwaySegmentLegnth;
                    rightBr.isHighway = true;
                    rightBr.theta = DiverseDirectionRandomly(initialTheta - Mathf.PI * 0.5f, highwayRandomDirRange);
                    rightBr.CalcEndPoint();

                    rightBr.branchCountDown = highwayBranchingMinInterval;
                    r.branchCountDown = highwayBranchingMinInterval;

                    queue.Insert(rightBr.t, rightBr);
                }
            }
        }

        // make street branchces
        if (populationSample > streetBranchingPopulation)
        {
            if (Random.value < streetBranchingProbability)
            {
                // make a left branch
                Road leftBr = new Road();
                leftBr.t = r.t + 3;
                leftBr.parent = r0.id;
                leftBr.startPoint = r.startPoint;
                leftBr.length = streetSegementLength;
                leftBr.isHighway = false;
                leftBr.theta = DiverseDirectionRandomly(initialTheta + Mathf.PI * 0.5f, streetRandomDirRange);
                leftBr.CalcEndPoint();

                r.branchCountDown = highwayBranchingMinInterval;

                queue.Insert(leftBr.t, leftBr);
            }
            else if (Random.value < streetBranchingProbability)
            {
                // make a right branch
                Road rightBr = new Road();
                rightBr.t = r.t + 3;
                rightBr.parent = r0.id;
                rightBr.startPoint = r.startPoint;
                rightBr.length = streetSegementLength;
                rightBr.isHighway = false;
                rightBr.theta = DiverseDirectionRandomly(initialTheta - Mathf.PI * 0.5f, streetRandomDirRange);
                rightBr.CalcEndPoint();

                r.branchCountDown = highwayBranchingMinInterval;

                queue.Insert(rightBr.t, rightBr);
            }
        }

    }

    IEnumerator PlaceBuildings()
    {
        float gap = baseAllotmentSize * 0.2f;

        foreach (Segment seg in segments)
        {
            if (seg.isHighway)
                continue;

            Vector2 dir = seg.end1 - seg.end0;
            float segmentLength = dir.magnitude;
            dir.Normalize();

            Vector2 sideDir = new Vector2(-dir.y, dir.x);

            Vector2 midPoint = (seg.end0 + seg.end1) * 0.5f;
            float pop = populationMap.Sample(midPoint.x / width, midPoint.y / height, 0.0f);

            float allotmentSize = baseAllotmentSize;
            if (pop > bigAllotmentPopThreshold)
            {
                allotmentSize *= 1.0f + (pop - bigAllotmentPopThreshold) * bigAllotmentCoeff;
            }

            float halfAllotmentSize = allotmentSize * 0.5f;

            int steps = Mathf.FloorToInt(segmentLength / (allotmentSize + gap));
            float stepSize = segmentLength / steps;

            if (stepSize <= allotmentSize)
            {
                steps--;
                if (steps <= 0)
                    continue;

                stepSize = segmentLength / steps;
            }

            for (int s = 0; s < steps; s++)
            {
                float t = stepSize * (s + 0.5f);

                // try add allotment on one side
                {
                    Vector2 center = seg.end0 + dir * t + stepSize * 0.5f * sideDir;
                    SAT.Rect r = new SAT.Rect()
                    {
                        center = center,
                        halfExtends = new Vector2(halfAllotmentSize, halfAllotmentSize),
                        theta = seg.theta
                    };

                    if (AdjustAllotment(r))
                    {
                        allotments.Add(r);

                        if (allotmentTemplate)
                        {
                            GameObject go = Instantiate(allotmentTemplate);
                            go.transform.localPosition = new Vector3(r.center.x, 0.0f, r.center.y) + origin;
                            go.transform.localScale = new Vector3(allotmentSize, allotmentSize, 1);
                            go.transform.localRotation = Quaternion.Euler(90.0f, -Mathf.Rad2Deg * r.theta, 0.0f);
                            go.SetActive(true);
                        }
                    }
                }

                yield return null;

                // try add allotment on another side
                {
                    Vector2 center = seg.end0 + dir * t - stepSize * 0.5f * sideDir;
                    SAT.Rect r = new SAT.Rect()
                    {
                        center = center,
                        halfExtends = new Vector2(halfAllotmentSize, halfAllotmentSize),
                        theta = seg.theta
                    };

                    if (AdjustAllotment(r))
                    {
                        allotments.Add(r);

                        if (allotmentTemplate)
                        {
                            GameObject go = Instantiate(allotmentTemplate);
                            go.transform.localPosition = new Vector3(r.center.x, 0.0f, r.center.y) + origin;
                            go.transform.localScale = new Vector3(allotmentSize, allotmentSize, 1);
                            go.transform.localRotation = Quaternion.Euler(90.0f, -Mathf.Rad2Deg * r.theta, 0.0f);
                            go.SetActive(true);
                        }
                    }
                }
            }

            yield return null;
        }
    }

    bool AdjustAllotment(SAT.Rect r)
    {
        foreach(Segment s in segments)
        {
            if (SAT.Test(r, s))
                return false;
        }

        foreach(SAT.Rect a in allotments)
        {
            if (SAT.Test(r, a))
                return false;
        }

        return true;
    }
}
