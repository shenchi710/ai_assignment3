﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class Test : Editor
{

    [MenuItem("Tests/Generate 2D Perlin Noise")]
    public static void GeneratePerlinNoise()
    {
        int res = 512;
        Texture2D tex = new Texture2D(res, res, TextureFormat.RGBA32, false);
        
        for (int y = 0; y < res; y++)
        {
            for (int x = 0; x < res; x++)
            {
                float v = PerlinNoise.Noise(x * 8.0f / res, y * 8.0f / res) * 0.5f + 0.5f;
                tex.SetPixel(x, y, new Color(v, v, v, 1));
            }
        }

        string filepath = Application.dataPath + "/Resources/perlin2d.png";
        Debug.Log("write to " + filepath);

        File.WriteAllBytes(filepath, tex.EncodeToPNG());

    }

    [MenuItem("Tests/Generate Voronoi Map")]
    public static void GenerateVoronoi()
    {
        int N = 16;
        int res = 512;

        Random.InitState(0);

        Texture2D tex = new Texture2D(res, res, TextureFormat.RGBA32, false);

        Color[,] map = new Color[res, res];

        Vector2[] points = new Vector2[N];
        Color[] values = new Color[N];

        for (int i = 0; i < N; i++)
        {
            points[i] = new Vector2(Random.Range(0.0f, (float)res), Random.Range(0.0f, (float)res));
            values[i] = new Color(Random.value, Random.value, Random.value, 1.0f);
        }

        Voronoi.Generate(map, points, values);

        for (int y = 0; y < res; y++)
        {
            for (int x = 0; x < res; x++)
            {
                tex.SetPixel(x, y, map[y, x]);
            }
        }

        string filepath = Application.dataPath + "/Resources/voronoi.png";
        Debug.Log("write to " + filepath);

        File.WriteAllBytes(filepath, tex.EncodeToPNG());

    }

}
