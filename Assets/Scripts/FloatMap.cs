﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatMap
{
    public int Width { get; private set; }
    public int Height { get; private set; }

    private float[,] map;

    public FloatMap(int width, int height)
    {
        Width = width;
        Height = height;
        map = new float[height, width];
    }

    public void FillInPerlinNoise(float scaleX = 8, float scaleY = 8, float offsetX = 0, float offsetY = 0, System.Func<float, float> modifier = null)
    {
        if (null == modifier)
        {
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    float v = PerlinNoise.Noise(
                        offsetX + x * scaleX / Width,
                        offsetY + y * scaleY / Height);
                    map[y, x] = v;
                }
            }
        }
        else
        {
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    float v = PerlinNoise.Noise(
                        offsetX + x * scaleX / Width,
                        offsetY + y * scaleY / Height);
                    map[y, x] = modifier(v);
                }
            }
        }
    }
    
    public float Sample(float x, float y)
    {
        x = Mathf.Clamp01(x);
        y = Mathf.Clamp01(y);
        return map[Mathf.RoundToInt(y * (Height - 1)),
            Mathf.RoundToInt(x * (Width - 1))];
    }

    public float Sample(float x, float y, float borderValue)
    {
        if (x < 0.0f || x > 1.0f || y < 0.0f || y > 1.0f)
            return borderValue;

        return map[Mathf.RoundToInt(y * (Height - 1)),
            Mathf.RoundToInt(x * (Width - 1))];
    }

    public Texture2D CreateTexture(Color tint)
    {
        Texture2D tex = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
        
        for (int y = 0; y < Height; y++)
        {
            for (int x = 0; x < Width; x++)
            {
                float v = map[y, x];
                tex.SetPixel(x, y, tint * v);
            }
        }

        tex.Apply();

        return tex;
    }

}
