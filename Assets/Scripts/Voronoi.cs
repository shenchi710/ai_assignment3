﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Voronoi
{
    public static void Generate<T>(T[,] heightMap, Vector2[] points, T[] values)
    {
        int height = heightMap.GetUpperBound(0) + 1;
        int width = heightMap.GetUpperBound(1) + 1;

        Vector3 texelCoord = Vector3.zero;
        Vector3 siteCoord = Vector3.zero;

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                float minDist = float.MaxValue;
                int siteID = -1;

                for (int i = 0; i < points.Length; i++)
                {
                    texelCoord.x = x;
                    texelCoord.y = y;
                    siteCoord.x = points[i].x;
                    siteCoord.y = points[i].y;

                    float dist = (texelCoord - siteCoord).magnitude;
                    if (dist < minDist)
                    {
                        minDist = dist;
                        siteID = i;
                    }
                }

                heightMap[y, x] = values[siteID];
            }
        }
    }

    public static void Generate<T>(T[,] heightMap, Vector2[] points, Func<int, float, T> func)
    {
        int height = heightMap.GetUpperBound(0) + 1;
        int width = heightMap.GetUpperBound(1) + 1;

        Vector3 texelCoord = Vector3.zero;
        Vector3 siteCoord = Vector3.zero;

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                float minDist = float.MaxValue;
                int siteID = -1;

                for (int i = 0; i < points.Length; i++)
                {
                    texelCoord.x = x;
                    texelCoord.y = y;
                    siteCoord.x = points[i].x;
                    siteCoord.y = points[i].y;

                    float dist = (texelCoord - siteCoord).magnitude;
                    if (dist < minDist)
                    {
                        minDist = dist;
                        siteID = i;
                    }
                }

                heightMap[y, x] = func(siteID, minDist);
            }
        }
    }


}
