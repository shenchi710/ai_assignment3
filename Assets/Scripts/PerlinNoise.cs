﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerlinNoise
{

    static Vector3[] gradientTable3D = null;
    static Vector2[] gradientTable2D = null;

    static int[] indexTable = null;

    // initialize gradientTable and indexTable
    static PerlinNoise()
    {
        gradientTable3D = new Vector3[256];
        gradientTable2D = new Vector2[256];

        indexTable = new int[512];

        // fill in gradientTable3D
        for (int i = 0; i < 256; i++)
        {
            do
            {
                gradientTable3D[i] = new Vector3(
                    Random.Range(-1.0f, 1.0f),
                    Random.Range(-1.0f, 1.0f),
                    Random.Range(-1.0f, 1.0f));
            }
            while (gradientTable3D[i].sqrMagnitude > 1.0f);

            gradientTable3D[i].Normalize();
        }

        // fill in gradientTable2D
        for (int i = 0; i < 256; i++)
        {
            do
            {
                gradientTable2D[i] = new Vector2(
                    Random.Range(-1.0f, 1.0f),
                    Random.Range(-1.0f, 1.0f));
            }
            while (gradientTable2D[i].sqrMagnitude > 1.0f);

            gradientTable2D[i].Normalize();
        }

        // fill in first half of indexTable
        for (int i = 0; i < 256; i++)
        {
            indexTable[i] = i;
        }

        // shuffle first half of indexTable
        for (int i = 0; i < 256; i++)
        {
            int j = Random.Range(0, 256);

            // swap ith and jth element
            int t = indexTable[i];
            indexTable[i] = indexTable[j];
            indexTable[j] = t;
        }

        // clone first half of indexTable to the second
        for (int i = 0; i < 256; i++)
        {
            indexTable[i + 256] = indexTable[i];
        }
    }


    // Fold 2D coordinate to index
    private static int Fold(int x, int y)
    {
        return indexTable[indexTable[x] + y];
    }

    // Fold 3D coordinate to index
    private static int Fold(int x, int y, int z)
    {
        return indexTable[indexTable[indexTable[x] + y] + z];
    }

    // dropoff function (-1 <= t <= 1)
    public static float Drop(float t)
    {
        return t * t * t * (t * (t * 6 - 15) + 10);
    }

    // Compute wavelet at a corner
    private static float Wavelet(float x, float y, int iX, int iY)
    {
        float u = x - iX;
        float v = y - iY;
        return Vector2.Dot(gradientTable2D[Fold(iX & 0xff, iY & 0xff)], new Vector2(u, v));
    }

    // Compute wavelet at a corner
    private static float Wavelet(float x, float y, float z, int iX, int iY, int iZ)
    {
        float u = x - iX;
        float v = y - iY;
        float w = z - iZ;
        return Vector3.Dot(gradientTable3D[Fold(iX & 0xff, iY & 0xff, iZ & 0xff)], new Vector3(u, v, w));
    }

    // 2D Perlin Noise
    public static float Noise(float x, float y)
    {
        x = Mathf.Repeat(x, 256.0f);
        y = Mathf.Repeat(y, 256.0f);

        // lower corner coordinates
        int lx = Mathf.FloorToInt(x);
        int ly = Mathf.FloorToInt(y);
        
        float c00 = Wavelet(x, y, lx/**/, ly/**/);
        float c01 = Wavelet(x, y, lx + 1, ly/**/);
        float c10 = Wavelet(x, y, lx/**/, ly + 1);
        float c11 = Wavelet(x, y, lx + 1, ly + 1);

        float u = Drop(x - lx);
        float v = Drop(y - ly);

        float c0 = Mathf.Lerp(c00, c10, v);
        float c1 = Mathf.Lerp(c01, c11, v);

        return Mathf.Lerp(c0, c1, u);
    }
    
    // 3D Perlin Noise
    public static float Noise(float x, float y, float z)
    {
        x = Mathf.Repeat(x, 256.0f);
        y = Mathf.Repeat(y, 256.0f);
        z = Mathf.Repeat(z, 256.0f);

        // lower corner coordinates
        int lx = Mathf.FloorToInt(x);
        int ly = Mathf.FloorToInt(y);
        int lz = Mathf.FloorToInt(z);
        
        float c000 = Wavelet(x, y, z, lx/**/, ly/**/, lz/**/);
        float c001 = Wavelet(x, y, z, lx + 1, ly/**/, lz/**/);
        float c010 = Wavelet(x, y, z, lx/**/, ly + 1, lz/**/);
        float c011 = Wavelet(x, y, z, lx + 1, ly + 1, lz/**/);
        float c100 = Wavelet(x, y, z, lx/**/, ly/**/, lz + 1);
        float c101 = Wavelet(x, y, z, lx + 1, ly/**/, lz + 1);
        float c110 = Wavelet(x, y, z, lx/**/, ly + 1, lz + 1);
        float c111 = Wavelet(x, y, z, lx + 1, ly + 1, lz + 1);

        float u = Drop(x - lx);
        float v = Drop(y - ly);
        float w = Drop(z - lz);

        float c00 = Mathf.Lerp(c000, c100, w);
        float c01 = Mathf.Lerp(c001, c101, w);
        float c10 = Mathf.Lerp(c010, c110, w);
        float c11 = Mathf.Lerp(c011, c111, w);

        float c0 = Mathf.Lerp(c00, c10, v);
        float c1 = Mathf.Lerp(c01, c11, v);

        return Mathf.Lerp(c0, c1, u);
    }

}
