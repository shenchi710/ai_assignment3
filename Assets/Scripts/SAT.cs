﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 2D version of separating axis therom
public class SAT
{
    public class Rect
    {
        public Vector2 center;
        public Vector2 halfExtends;
        public float theta;
    }

    public class Segment
    {
        public Vector2 end0;
        public Vector2 end1;
    }

    private static Vector2 Rotate(Vector2 pos, float theta)
    {
        float c = Mathf.Cos(theta);
        float s = Mathf.Sin(theta);

        return new Vector2(
            pos.x * c - pos.y * s,
            pos.y * c + pos.x * s
            );
    }

    private static Vector2 LeftBottom(Rect r)
    {
        return r.center + Rotate(-r.halfExtends, r.theta);
    }

    private static Vector2 RightBottom(Rect r)
    {
        Vector2 delta = new Vector2(r.halfExtends.x, -r.halfExtends.y);
        return r.center + Rotate(delta, r.theta);
    }

    private static Vector2 LeftTop(Rect r)
    {
        Vector2 delta = new Vector2(-r.halfExtends.x, r.halfExtends.y);
        return r.center + Rotate(delta, r.theta);
    }

    private static Vector2 RightTop(Rect r)
    {
        return r.center + Rotate(r.halfExtends, r.theta);
    }

    private static bool TestAToB(Rect a, Rect b)
    {
        Vector2 dir = new Vector2(Mathf.Cos(a.theta), Mathf.Sin(a.theta));

        Vector2 a_leftBottom = LeftBottom(a);
        Vector2 a_rightTop = RightTop(a);

        Vector2 b_leftBottom = LeftBottom(b);
        Vector2 b_rightBottom = RightBottom(b);
        Vector2 b_leftTop = LeftTop(b);
        Vector2 b_rightTop = RightTop(b);

        {
            float a_min = Vector2.Dot(a_leftBottom, dir);
            float a_max = Vector2.Dot(a_rightTop, dir);

            float b_1 = Vector2.Dot(b_leftBottom, dir);
            float b_2 = Vector2.Dot(b_rightBottom, dir);
            float b_3 = Vector2.Dot(b_leftTop, dir);
            float b_4 = Vector2.Dot(b_rightTop, dir);

            float b_min = Mathf.Min(Mathf.Min(b_1, b_2), Mathf.Min(b_3, b_4));
            float b_max = Mathf.Max(Mathf.Max(b_1, b_2), Mathf.Max(b_3, b_4));

            if (a_min > b_max || b_min > a_max)
                return false;
        }

        dir = new Vector2(-dir.y, dir.x);
        {
            float a_min = Vector2.Dot(a_leftBottom, dir);
            float a_max = Vector2.Dot(a_rightTop, dir);

            float b_1 = Vector2.Dot(b_leftBottom, dir);
            float b_2 = Vector2.Dot(b_rightBottom, dir);
            float b_3 = Vector2.Dot(b_leftTop, dir);
            float b_4 = Vector2.Dot(b_rightTop, dir);

            float b_min = Mathf.Min(Mathf.Min(b_1, b_2), Mathf.Min(b_3, b_4));
            float b_max = Mathf.Max(Mathf.Max(b_1, b_2), Mathf.Max(b_3, b_4));

            if (a_min > b_max || b_min > a_max)
                return false;
        }

        return true;
    }

    public static bool Test(Rect a, Rect b)
    {
        return TestAToB(a, b) && TestAToB(b, a);
    }

    public static bool Test(Rect a, Segment b)
    {
        Vector2 dir = new Vector2(Mathf.Cos(a.theta), Mathf.Sin(a.theta));

        Vector2 a_leftBottom = LeftBottom(a);
        Vector2 a_rightBottom = RightBottom(a);
        Vector2 a_leftTop = LeftTop(a);
        Vector2 a_rightTop = RightTop(a);

        {
            float a_min = Vector2.Dot(a_leftBottom, dir);
            float a_max = Vector2.Dot(a_rightTop, dir);

            float b_1 = Vector2.Dot(b.end0, dir);
            float b_2 = Vector2.Dot(b.end1, dir);

            float b_min = Mathf.Min(b_1, b_2);
            float b_max = Mathf.Max(b_1, b_2);

            if (a_min > b_max || b_min > a_max)
                return false;
        }

        dir = new Vector2(-dir.y, dir.x);

        {
            float a_min = Vector2.Dot(a_leftBottom, dir);
            float a_max = Vector2.Dot(a_rightTop, dir);

            float b_1 = Vector2.Dot(b.end0, dir);
            float b_2 = Vector2.Dot(b.end1, dir);

            float b_min = Mathf.Min(b_1, b_2);
            float b_max = Mathf.Max(b_1, b_2);

            if (a_min > b_max || b_min > a_max)
                return false;
        }

        dir = (b.end1 - b.end0).normalized;
        dir = new Vector2(-dir.y, dir.x);

        {
            float a_1 = Vector2.Dot(a_leftBottom, dir);
            float a_2 = Vector2.Dot(a_rightBottom, dir);
            float a_3 = Vector2.Dot(a_leftTop, dir);
            float a_4 = Vector2.Dot(a_rightTop, dir);

            float a_min = Mathf.Min(Mathf.Min(a_1, a_2), Mathf.Min(a_3, a_4));
            float a_max = Mathf.Max(Mathf.Max(a_1, a_2), Mathf.Max(a_3, a_4));

            float b_m = Vector2.Dot(b.end0, dir);

            if (a_min > b_m || b_m > a_max)
                return false;
        }

        return true;
    }
}
