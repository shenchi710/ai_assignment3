﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Modifiers
{

    public static float fBM(Func<float, float , float> func, float x, float y, float amplitudeMultiplier = 0.5f, float frequencyMultiplier = 2.0f, int numIteration = 4)
    {
        float ration = 1.0f;
        float amplitude = amplitudeMultiplier;
        float frequency = 1.0f;
        float ret = 0.0f;

        for (int i = 0; i < numIteration; i++)
        {
            ret += amplitude * func(x * frequency, y * frequency);
            ration -= amplitude;
            amplitude = ration * amplitudeMultiplier;
            frequency *= frequencyMultiplier;
        }

        ret += ration * func(x * frequency, y * frequency);

        return ret;
    }

    public static float fBM(float[,] map, int x, int y, float amplitudeMultiplier = 0.5f, float frequencyMultiplier = 2.0f, int numIteration = 4)
    {
        float ration = 1.0f;
        float amplitude = amplitudeMultiplier;
        float frequency = 1.0f;
        float ret = 0.0f;

        for (int i = 0; i < numIteration; i++)
        {
            ret += amplitude * map[Mathf.FloorToInt(x * frequency), Mathf.FloorToInt(y * frequency)];
            ration -= amplitude;
            amplitude = ration * amplitudeMultiplier;
            frequency *= frequencyMultiplier;
        }

        ret += ration * map[Mathf.FloorToInt(x * frequency), Mathf.FloorToInt(y * frequency)];

        return ret;
    }

    public static void fBM(float[,] dstMap, float[,] srcMap, float amplitudeMultiplier = 0.5f, float frequencyMultiplier = 2.0f, int numIteration = 4)
    {
        int height = dstMap.GetUpperBound(0);
        int width = dstMap.GetUpperBound(1);
        if (height != dstMap.GetUpperBound(0) || width != srcMap.GetUpperBound(1))
        {
            return;
        }

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                float ration = 1.0f;
                float amplitude = amplitudeMultiplier;
                float frequency = 1.0f;
                float ret = 0.0f;
                
                for (int i = 0; i < numIteration; i++)
                {
                    ret += amplitude * srcMap[Mathf.FloorToInt(x * frequency), Mathf.FloorToInt(y * frequency)];
                    ration -= amplitude;
                    amplitude = ration * amplitudeMultiplier;
                    frequency *= frequencyMultiplier;
                }

                ret += ration * srcMap[Mathf.FloorToInt(x * frequency), Mathf.FloorToInt(y * frequency)];

                dstMap[y, x] = ret;
            }
        }

        return;
    }

    public static void Blur(float[,] dstMap, float[,] srcMap, int radius)
    {
        int height = dstMap.GetUpperBound(0);
        int width = dstMap.GetUpperBound(1);
        if (height != dstMap.GetUpperBound(0) || width != srcMap.GetUpperBound(1))
        {
            return;
        }

        float[,] weights = new float[2 * radius + 1, 2 * radius + 1];
        float weightSum = 0.0f;
        for (int y = -radius; y <= radius; y++)
        {
            for (int x = -radius; x <= radius; x++)
            {
                weights[y + radius, x + radius] = 1.0f / (1.0f + Mathf.Sqrt(x * x + y * y));
                weightSum += weights[y + radius, x + radius];
            }
        }

        for (int y = -radius; y <= radius; y++)
        {
            for (int x = -radius; x <= radius; x++)
            {
                weights[y + radius, x + radius] /= weightSum;
            }
        }

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {

                dstMap[y, x] = 0.0f;

                for (int dy = -radius; dy <= radius; dy++)
                {
                    for (int dx = -radius; dx <= radius; dx++)
                    {
                        int sy = Mathf.Clamp(y + dy, 0, height - 1);
                        int sx = Mathf.Clamp(x + dx, 0, width - 1);
                        dstMap[y, x] += weights[dy + radius, dx + radius] * srcMap[sy, sx];
                    }
                }

            }
        }
    }
}
