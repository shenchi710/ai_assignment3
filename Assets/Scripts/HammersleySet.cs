﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Hammersley Point Set
// reference: http://holger.dammertz.org/stuff/notes_HammersleyOnHemisphere.html
public class Hammersley
{
    private static float radicalInverse_VdC(uint bits)
    {
        bits = (bits << 16) | (bits >> 16);
        bits = ((bits & 0x55555555u) << 1) | ((bits & 0xAAAAAAAAu) >> 1);
        bits = ((bits & 0x33333333u) << 2) | ((bits & 0xCCCCCCCCu) >> 2);
        bits = ((bits & 0x0F0F0F0Fu) << 4) | ((bits & 0xF0F0F0F0u) >> 4);
        bits = ((bits & 0x00FF00FFu) << 8) | ((bits & 0xFF00FF00u) >> 8);
        return (float)bits * 2.3283064365386963e-10f; // / 0x100000000
    }

    public static Vector2 Point(uint i, uint N)
    {
        return new Vector2((float)i / N, radicalInverse_VdC(i));
    }

}
